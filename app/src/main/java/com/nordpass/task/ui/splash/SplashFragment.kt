package com.nordpass.task.ui.splash

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.nordpass.task.R
import com.nordpass.task.databinding.FragmentSplashBinding
import com.nordpass.task.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : BaseFragment(R.layout.fragment_splash) {
    private val viewModel: SplashViewModel by viewModels()

    private val binding: FragmentSplashBinding by lazy { FragmentSplashBinding.bind(requireView()) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeState()
    }

    private fun observeState() {
        viewModel.state.observe(
            viewLifecycleOwner
        ) { state ->
            binding.progressBar.isVisible = state.isLoading
            state.showTodoView?.getContentIfNotHandled()?.let {
                showTodoList()
            }
            state.error?.getContentIfNotHandled()?.let {
                showError(it)
            }
        }
    }

    private fun showTodoList() {
        findNavController().navigate(SplashFragmentDirections.actionTodoList())
    }
}