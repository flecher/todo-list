package com.nordpass.task.ui.edit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.nordpass.task.utils.Constants
import com.nordpass.task.utils.Event
import com.nordpass.task.utils.SimpleEvent
import com.nordpass.task.utils.getValue
import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.todoedit.UpdateTodoTitleUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class TodoEditViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val updateTodoTitleUseCase: UpdateTodoTitleUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _state = MutableLiveData(ViewState())
    val state: LiveData<ViewState> get() = _state

    private val todo by savedStateHandle.getValue<Todo>(Constants.TODO_ARG)

    private var newTitle: String = ""

    init {
        onReduceState(ViewActions.ReceivedItem(todo))
    }

    fun onTextChanged(text: String) {
        onReduceState(ViewActions.ReceivedNewText(text))
    }

    fun tappedSave() {
        onReduceState(ViewActions.TappedSave)
    }

    private fun onReduceState(viewAction: ViewActions) {
        when (viewAction) {
            is ViewActions.ReceivedItem -> {
                newTitle = viewAction.todo.title
                _state.value = _state.value!!.copy(
                    item = viewAction.todo,
                    title = Event(viewAction.todo.title)
                )
            }
            is ViewActions.ReceivedNewText -> {
                if (_state.value!!.isEmptyError && viewAction.text.isBlank()) {
                    _state.value = _state.value!!.copy(isEmptyError = true)
                } else {
                    _state.value = _state.value!!.copy(isEmptyError = false)
                }
                newTitle = viewAction.text
            }
            ViewActions.TappedSave -> {
                if (newTitle.isBlank()) {
                    _state.value = _state.value!!.copy(isEmptyError = true)
                } else {
                    val id = _state.value!!.item
                    id?.let {
                        updateTodoTitleUseCase(it.id, newTitle)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .onErrorComplete()
                            .doOnComplete {
                                _state.value = _state.value!!.copy(goBack = SimpleEvent())
                            }
                            .subscribe()
                            .addTo(disposable)
                    }
                }
            }
        }
    }

    sealed class ViewActions {
        data class ReceivedItem(val todo: Todo) : ViewActions()
        data class ReceivedNewText(val text: String) : ViewActions()
        object TappedSave : ViewActions()
    }

    data class ViewState(
        val item: Todo? = null,
        val title: Event<String> = Event(""),
        val isEmptyError: Boolean = false,
        val goBack: SimpleEvent? = null
    )

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}