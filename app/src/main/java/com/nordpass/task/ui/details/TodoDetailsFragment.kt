package com.nordpass.task.ui.details

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nordpass.task.R
import com.nordpass.task.databinding.FragmentDetailsBinding
import com.nordpass.task.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TodoDetailsFragment : BaseFragment(R.layout.fragment_details) {
    private val viewModel: TodoDetailsViewModel by viewModels()

    private lateinit var binding: FragmentDetailsBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailsBinding.bind(view)
        binding.detailsEdit.setOnClickListener {
            viewModel.onEditClicked()
        }
        binding.detailsTodo.setOnClickListener {
            viewModel.onTodoClicked()
        }
        binding.detailsFinish.setOnClickListener {
            viewModel.onFinishedClicked()
        }
        observeState()
    }

    private fun observeState() {
        viewModel.state.observe(
            viewLifecycleOwner
        ) { state ->
            state.item?.let {
                with(it) {
                    binding.detailsTitle.text = title
                    binding.detailsStatus.text = if (isCompleted) {
                        getString(R.string.completedLabel)
                    } else {
                        getString(R.string.unfinishedLabel)
                    }
                    binding.detailsTodo.isVisible = isCompleted
                    binding.detailsFinish.isVisible = !isCompleted
                }
            }
            state.navAction?.getContentIfNotHandled()?.let {
                when (it) {
                    TodoDetailsViewModel.NavAction.GoBack -> findNavController().navigateUp()
                    is TodoDetailsViewModel.NavAction.GoToEdit -> {
                        val action = TodoDetailsFragmentDirections.actionTodoEdit(it.todo)
                        findNavController().navigate(action)
                    }
                }
            }
            state.error?.getContentIfNotHandled()?.let {
                showError(it)
            }
        }
    }
}