package com.nordpass.task.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nordpass.task.ui.base.ErrorMessage
import com.nordpass.task.ui.base.ErrorMessageMapper
import com.nordpass.task.utils.Event
import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.todolist.GetTodoListUseCase
import com.nordpass.tt.usecase.todolist.SortTodoListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class TodoListViewModel @Inject constructor(
    getTodoListUseCase: GetTodoListUseCase,
    private val sortTodoListUseCase: SortTodoListUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _state = MutableLiveData(ViewState())
    val state: LiveData<ViewState> get() = _state

    init {
        getTodoListUseCase.observe()
            .map { sortTodoListUseCase(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    onReduceState(ViewActions.ReceivedItems(it))
                }, {
                    onReduceState(ViewActions.ReceivedError(it))
                }
            ).addTo(disposable)
    }

    fun onItemClicked(todo: Todo) {
        onReduceState(ViewActions.TappedItem(todo))
    }

    private fun onReduceState(action: ViewActions) {
        when (action) {
            is ViewActions.ReceivedItems -> {
                _state.value = _state.value!!.copy(items = action.items)
            }
            is ViewActions.ReceivedError -> {
                _state.value =
                    _state.value!!.copy(showError = Event(ErrorMessageMapper.map(action.error)))
            }
            is ViewActions.TappedItem -> {
                _state.value = _state.value!!.copy(showItem = Event(action.item))
            }
        }
    }

    sealed class ViewActions {
        data class ReceivedItems(val items: List<Todo>) : ViewActions()
        data class ReceivedError(val error: Throwable) : ViewActions()
        data class TappedItem(val item: Todo) : ViewActions()
    }

    data class ViewState(
        val items: List<Todo> = emptyList(),
        val showItem: Event<Todo>? = null,
        val showError: Event<ErrorMessage>? = null
    )

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}