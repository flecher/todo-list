package com.nordpass.task.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nordpass.task.ui.base.ErrorMessage
import com.nordpass.task.ui.base.ErrorMessageMapper
import com.nordpass.task.utils.Event
import com.nordpass.task.utils.SimpleEvent
import com.nordpass.tt.usecase.todolist.SyncTodoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    syncTodoUseCase: SyncTodoUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _state = MutableLiveData(ViewState())
    val state: LiveData<ViewState> get() = _state

    init {
        syncTodoUseCase.sync()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                onReduceState(ViewActions.LoadingChanged(true))
            }
            .doOnComplete {
                onReduceState(ViewActions.LoadingChanged(false))
            }
            .doOnError {
                onReduceState(ViewActions.LoadingChanged(false))
            }
            .subscribeBy(
                onComplete = { onReduceState(ViewActions.ShowTodoView) },
                onError = { onReduceState(ViewActions.ReceivedError(it)) }
            )
            .addTo(disposable)
    }

    private fun onReduceState(viewAction: ViewActions) {
        when (viewAction) {
            is ViewActions.LoadingChanged -> {
                _state.value = _state.value!!.copy(isLoading = viewAction.isLoading)
            }
            ViewActions.ShowTodoView -> {
                _state.value = _state.value!!.copy(showTodoView = SimpleEvent())
            }
            is ViewActions.ReceivedError -> {
                _state.value =
                    _state.value?.copy(error = Event(ErrorMessageMapper.map(viewAction.error)))
            }
        }
    }

    sealed class ViewActions {
        data class LoadingChanged(val isLoading: Boolean) : ViewActions()
        data class ReceivedError(val error: Throwable) : ViewActions()
        object ShowTodoView : ViewActions()
    }

    data class ViewState(
        val isLoading: Boolean = false,
        val showTodoView: SimpleEvent? = null,
        val error: Event<ErrorMessage>? = null
    )

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}