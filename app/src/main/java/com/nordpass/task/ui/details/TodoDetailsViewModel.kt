package com.nordpass.task.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.nordpass.task.ui.base.ErrorMessage
import com.nordpass.task.ui.base.ErrorMessageMapper
import com.nordpass.task.utils.Constants.TODO_ARG
import com.nordpass.task.utils.Event
import com.nordpass.task.utils.getValue
import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.tododetails.GetTodoUseCase
import com.nordpass.tt.usecase.tododetails.UpdateTodoStatusUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class TodoDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    getTodoUseCase: GetTodoUseCase,
    private val updateTodoStatusUseCase: UpdateTodoStatusUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _state = MutableLiveData(ViewState())
    val state: LiveData<ViewState> get() = _state

    private val todo by savedStateHandle.getValue<Todo>(TODO_ARG)

    init {
        getTodoUseCase(todo.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { todo ->
                    onReduceState(ViewActions.ReceivedItem(todo))
                },
                onError = {
                    onReduceState(ViewActions.ReceivedError(it))
                }
            )
            .addTo(disposable)
    }

    fun onFinishedClicked() {
        onReduceState(ViewActions.TappedFinish)
    }

    fun onTodoClicked() {
        onReduceState(ViewActions.TappedTodo)
    }

    fun onEditClicked() {
        onReduceState(ViewActions.TappedEdit)
    }

    private fun onReduceState(viewAction: ViewActions) {
        when (viewAction) {
            is ViewActions.ReceivedItem -> {
                _state.value = _state.value!!.copy(item = viewAction.todo)
            }
            ViewActions.TappedFinish -> {
                updateTodoStatusUseCase(todo.id, true)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorComplete()
                    .doOnComplete {
                        _state.value = _state.value!!.copy(navAction = Event(NavAction.GoBack))
                    }
                    .subscribe()
                    .addTo(disposable)
            }
            ViewActions.TappedTodo -> {
                updateTodoStatusUseCase(todo.id, false)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorComplete()
                    .doOnComplete {
                        _state.value = _state.value!!.copy(navAction = Event(NavAction.GoBack))
                    }
                    .subscribe()
                    .addTo(disposable)
            }
            ViewActions.TappedEdit -> {
                _state.value = _state.value!!.copy(navAction = Event(NavAction.GoToEdit(todo)))
            }
            is ViewActions.ReceivedError -> {
                _state.value = _state.value!!.copy(error = Event(ErrorMessageMapper.map(viewAction.error)))
            }
        }
    }

    sealed class ViewActions {
        data class ReceivedItem(val todo: Todo) : ViewActions()
        data class ReceivedError(val error: Throwable) : ViewActions()
        object TappedFinish : ViewActions()
        object TappedTodo : ViewActions()
        object TappedEdit : ViewActions()
    }

    sealed class NavAction {
        object GoBack : NavAction()
        data class GoToEdit(val todo: Todo) : NavAction()
    }

    data class ViewState(
        val item: Todo? = null,
        val navAction: Event<NavAction>? = null,
        val error: Event<ErrorMessage>? = null
    )

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}