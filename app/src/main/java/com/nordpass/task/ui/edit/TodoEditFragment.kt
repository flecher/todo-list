package com.nordpass.task.ui.edit

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.nordpass.task.R
import com.nordpass.task.databinding.FragmentEditBinding
import com.nordpass.task.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TodoEditFragment : BaseFragment(R.layout.fragment_edit) {

    private val viewModel: TodoEditViewModel by viewModels()

    private lateinit var binding: FragmentEditBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentEditBinding.bind(view)
        binding.editSave.setOnClickListener {
            viewModel.tappedSave()
        }
        binding.editText.doOnTextChanged { text, _, _, _ ->
            viewModel.onTextChanged(text.toString())
        }
        observeState()
    }

    private fun observeState() {
        viewModel.state.observe(
            viewLifecycleOwner
        ) { state ->
            state.title.getContentIfNotHandled()?.let {
                binding.editText.setText(it)
            }
            if (state.isEmptyError) {
                binding.editInputLayout.error = getString(R.string.editTextError)
            } else {
                binding.editInputLayout.error = null
            }
            state.goBack?.getContentIfNotHandled()?.let {
                findNavController().navigateUp()
            }
        }
    }
}