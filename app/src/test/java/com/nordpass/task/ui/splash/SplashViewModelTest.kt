package com.nordpass.task.ui.splash

import com.nordpass.tt.usecase.todolist.SyncTodoUseCase
import com.nordpass.utils.InstantExecutorExtension
import com.nordpass.utils.RxSchedulerExtension
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(value = [RxSchedulerExtension::class, InstantExecutorExtension::class])
class SplashViewModelTest {

    private lateinit var viewModel: SplashViewModel
    private val syncTodoUseCase = mockk<SyncTodoUseCase>()

    @Nested
    inner class ReceivedItems {

        @BeforeEach
        fun setUp() {
            every { syncTodoUseCase.sync() } returns Completable.complete()
            viewModel = SplashViewModel(syncTodoUseCase)
        }

        @Test
        fun isNotLoading() {
            assertEquals(false, viewModel.state.value!!.isLoading)
        }

        @Test
        fun showTodoListViewEventIsPresent() {
            assertNotNull(viewModel.state.value!!.showTodoView)
        }
    }

    @Nested
    inner class ReceivedError {

        @BeforeEach
        fun setUp() {
            every { syncTodoUseCase.sync() } returns Completable.error(Throwable("Error"))
            viewModel = SplashViewModel(syncTodoUseCase)
        }

        @Test
        fun isNotLoading() {
            assertEquals(false, viewModel.state.value!!.isLoading)
        }

        @Test
        fun showErrorIsPresent() {
            assertNotNull(viewModel.state.value!!.error)
        }
    }
}