package com.nordpass.task.ui.list

import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.todolist.GetTodoListUseCase
import com.nordpass.tt.usecase.todolist.SortTodoListUseCase
import com.nordpass.utils.InstantExecutorExtension
import com.nordpass.utils.RxSchedulerExtension
import io.mockk.every
import io.mockk.mockk
import io.reactivex.subjects.PublishSubject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(value = [RxSchedulerExtension::class, InstantExecutorExtension::class])
class TodoListViewModelTest {

    private lateinit var viewModel: TodoListViewModel
    private val getTodoLisUseCase = mockk<GetTodoListUseCase>()
    private val sortTodoListUseCase = mockk<SortTodoListUseCase>()

    private val todoListResultObserver = PublishSubject.create<List<Todo>>()

    @BeforeEach
    fun setUp() {
        every { getTodoLisUseCase.observe() } returns todoListResultObserver
        viewModel = TodoListViewModel(getTodoLisUseCase, sortTodoListUseCase)
    }

    @Nested
    inner class ReceivedTodoList {

        private val todo = mockk<Todo>()

        @BeforeEach
        fun setUp() {
            every { sortTodoListUseCase(any()) } returns listOf(todo)
            todoListResultObserver.onNext(listOf(todo))
        }

        @Test
        fun receivedItems() {
            assertEquals(1, viewModel.state.value!!.items.size)
        }

        @Nested
        inner class TappedItem {

            @BeforeEach
            fun setUp() {
                viewModel.onItemClicked(todo)
            }

            @Test
            fun showItemIsPresent() {
                assertEquals(todo, viewModel.state.value!!.showItem?.peekContent())
            }
        }
    }

    @Nested
    inner class ReceivedError {

        @BeforeEach
        fun setUp() {
            todoListResultObserver.onError(Throwable("Error"))
        }

        @Test
        fun errorIsPresent() {
            assertNotNull(viewModel.state.value!!.showError)
        }
    }
}