package com.nordpass.task.ui.details

import androidx.lifecycle.SavedStateHandle
import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.tododetails.GetTodoUseCase
import com.nordpass.tt.usecase.tododetails.UpdateTodoStatusUseCase
import com.nordpass.utils.InstantExecutorExtension
import com.nordpass.utils.RxSchedulerExtension
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(value = [RxSchedulerExtension::class, InstantExecutorExtension::class])
class TodoDetailsViewModelTest {

    private lateinit var viewModel: TodoDetailsViewModel
    private val savedStateHandle = mockk<SavedStateHandle>()
    private val getTodoUseCase = mockk<GetTodoUseCase>()
    private val updateTodoStatusUseCase = mockk<UpdateTodoStatusUseCase>()

    private val todoObserver = PublishSubject.create<Todo>()

    private val todo = mockk<Todo> {
        every { id } returns 1
    }

    @BeforeEach
    fun setUp() {
        every { savedStateHandle.get<Any>(any()) } returns todo
        every { getTodoUseCase(any()) } returns todoObserver
        viewModel = TodoDetailsViewModel(savedStateHandle, getTodoUseCase, updateTodoStatusUseCase)
    }

    @Nested
    inner class ReceivedItem {

        @BeforeEach
        fun setUp() {
            todoObserver.onNext(todo)
        }

        @Test
        fun itemIsPresent() {
            assertEquals(todo, viewModel.state.value!!.item)
        }

        @Nested
        inner class TappedFinish {

            @BeforeEach
            fun setUp() {
                every { updateTodoStatusUseCase(any(), any()) } returns Completable.complete()
                viewModel.onFinishedClicked()
            }

            @Test
            fun updateTodoStatusIsCalled() {
                verify { updateTodoStatusUseCase(1, true) }
            }

            @Test
            fun navActionIsGoBack() {
                assertEquals(
                    TodoDetailsViewModel.NavAction.GoBack,
                    viewModel.state.value!!.navAction?.peekContent()
                )
            }
        }

        @Nested
        inner class TappedTodo {

            @BeforeEach
            fun setUp() {
                every { updateTodoStatusUseCase(any(), any()) } returns Completable.complete()
                viewModel.onTodoClicked()
            }

            @Test
            fun updateTodoStatusIsCalled() {
                verify { updateTodoStatusUseCase(1, false) }
            }

            @Test
            fun navActionIsGoBack() {
                assertEquals(
                    TodoDetailsViewModel.NavAction.GoBack,
                    viewModel.state.value!!.navAction?.peekContent()
                )
            }
        }

        @Nested
        inner class TappedEdit {

            @BeforeEach
            fun setUp() {
                viewModel.onEditClicked()
            }

            @Test
            fun navActionIsGoToEdit() {
                assertEquals(
                    TodoDetailsViewModel.NavAction.GoToEdit(todo),
                    viewModel.state.value!!.navAction?.peekContent()
                )
            }
        }
    }

    @Nested
    inner class ReceivedError {

        @BeforeEach
        fun setUp() {
            todoObserver.onError(Throwable("Error"))
        }

        @Test
        fun errorIsPresent() {
            assertNotNull(viewModel.state.value!!.error)
        }
    }
}