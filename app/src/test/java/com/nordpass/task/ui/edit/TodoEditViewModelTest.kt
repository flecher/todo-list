package com.nordpass.task.ui.edit

import androidx.lifecycle.SavedStateHandle
import com.nordpass.task.utils.SimpleEvent
import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.todoedit.UpdateTodoTitleUseCase
import com.nordpass.utils.InstantExecutorExtension
import com.nordpass.utils.RxSchedulerExtension
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(value = [RxSchedulerExtension::class, InstantExecutorExtension::class])
class TodoEditViewModelTest {

    private lateinit var viewModel: TodoEditViewModel
    private val savedStateHandle = mockk<SavedStateHandle>()
    private val updateTodoTitleUseCase = mockk<UpdateTodoTitleUseCase>()

    private val todo = mockk<Todo> {
        every { id } returns 1
        every { title } returns "test title"
    }

    @BeforeEach
    fun setUp() {
        every { savedStateHandle.get<Any>(any()) } returns todo
        viewModel = TodoEditViewModel(savedStateHandle, updateTodoTitleUseCase)
    }

    @Test
    fun itemIsReceived() {
        assertEquals(todo, viewModel.state.value!!.item)
    }

    @Test
    fun titleIsSet() {
        assertEquals("test title", viewModel.state.value!!.title.peekContent())
    }

    @Nested
    inner class ReceivedNewText {

        @Nested
        inner class TextIsNotEmpty {

            @BeforeEach
            fun setUp() {
                viewModel.onTextChanged("new title")
            }

            @Test
            fun errorIsFalse() {
                assertEquals(false, viewModel.state.value!!.isEmptyError)
            }
        }

        @Nested
        inner class TextIsEmpty {

            @BeforeEach
            fun setUp() {
                viewModel.onTextChanged("")
            }

            @Test
            fun errorIsTrue() {
                assertEquals(false, viewModel.state.value!!.isEmptyError)
            }
        }
    }

    @Nested
    inner class TappedSave {

        @Nested
        inner class TextIsNotEmpty {

            @BeforeEach
            fun setUp() {
                every { updateTodoTitleUseCase(any(), any()) } returns Completable.complete()
                viewModel.onTextChanged("title")
                viewModel.tappedSave()
            }

            @Test
            fun updateTodoTitleIsCalled() {
                verify { updateTodoTitleUseCase(1, "title") }
            }

            @Test
            fun goBackIsPresent() {
                assertNotNull(viewModel.state.value!!.goBack)
            }
        }

        @Nested
        inner class TextIsEmpty {

            @BeforeEach
            fun setUp() {
                viewModel.onTextChanged("")
                viewModel.tappedSave()
            }

            @Test
            fun emptyErrorIsTrue() {
                assertEquals(true, viewModel.state.value!!.isEmptyError)
            }

            @Nested
            inner class ReceivedNewText {

                @Nested
                inner class TextIsNotEmpty {

                    @BeforeEach
                    fun setUp() {
                        viewModel.onTextChanged("new title")
                    }

                    @Test
                    fun errorIsFalse() {
                        assertEquals(false, viewModel.state.value!!.isEmptyError)
                    }
                }

                @Nested
                inner class TextIsEmpty {

                    @BeforeEach
                    fun setUp() {
                        viewModel.onTextChanged("")
                    }

                    @Test
                    fun errorIsTrue() {
                        assertEquals(true, viewModel.state.value!!.isEmptyError)
                    }
                }
            }
        }
    }
}