package com.nordpass.tt.storage.todo

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nordpass.tt.storage.todo.TodoDao.Companion.TABLE_NAME
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
internal interface TodoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrCreate(list: List<TodoEntity>): Completable

    @Query("SELECT * FROM $TABLE_NAME")
    fun getAll(): Single<List<TodoEntity>>

    @Query("SELECT * FROM $TABLE_NAME")
    fun observeAll(): Observable<List<TodoEntity>>

    @Query("DELETE FROM $TABLE_NAME WHERE id in (:ids)")
    fun remove(ids: List<Int>): Completable

    @Query("SELECT * FROM $TABLE_NAME WHERE id == :id")
    fun getById(id: Int): Observable<TodoEntity>

    @Query("SELECT * FROM $TABLE_NAME WHERE id == :id")
    fun observeById(id: Int): Flowable<TodoEntity>

    @Query("UPDATE $TABLE_NAME SET isCompleted = :completed, updatedAt = :updatedAt WHERE id == :id")
    fun updateStatusById(id: Int, completed: Boolean, updatedAt: String): Completable

    @Query("UPDATE $TABLE_NAME SET title = :title, updatedAt = :updatedAt WHERE id == :id")
    fun updateTitleById(id: Int, title: String, updatedAt: String): Completable

    companion object {
        const val TABLE_NAME = "todo"
    }
}