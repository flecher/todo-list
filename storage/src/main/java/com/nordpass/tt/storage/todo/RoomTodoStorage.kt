package com.nordpass.tt.storage.todo

import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.common.Io
import com.nordpass.tt.usecase.todolist.TodoStorage
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

internal class RoomTodoStorage @Inject constructor(
    private val dao: TodoDao,
    private val mapper: TodoMapper
) : TodoStorage {

    override fun save(todoList: List<Todo>): Completable {
        return dao.updateOrCreate(todoList.map(mapper::map))
    }

    override fun observeAll(): Observable<List<Todo>> {
        return dao.observeAll()
            .map { list -> list.map(mapper::map) }
    }

    override fun getAll(): Single<List<Todo>> {
        return dao.getAll()
            .map { list -> list.map(mapper::map) }
    }

    override fun getById(id: Int): Single<Todo> {
        return dao.getById(id)
            .singleOrError()
            .map(mapper::map)
    }

    override fun observeById(id: Int): Observable<Todo> {
        return dao.getById(id)
            .map(mapper::map)
    }

    override fun removeById(ids: List<Int>): Completable {
        return dao.remove(ids)
    }

    override fun updateStatusById(id: Int, completed: Boolean, updatedAt: String): Completable {
        return dao.updateStatusById(id, completed, updatedAt)
    }

    override fun updateTitleById(id: Int, title: String, updatedAt: String): Completable {
        return dao.updateTitleById(id, title, updatedAt)
    }
}