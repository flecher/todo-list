package com.nordpass.tt.usecase.tododetails

import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.todolist.TodoStorage
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class GetTodoUseCaseTest {

    private val storage = mockk<TodoStorage>()
    private val todo = mockk<Todo>()

    private lateinit var useCase: GetTodoUseCase
    private lateinit var observer: TestObserver<Todo>

    private val taskId = 1

    @BeforeEach
    fun setUp() {
        every { storage.observeById(any()) } returns Observable.just(todo)
        useCase = GetTodoUseCase(storage)
        observer = useCase(taskId).test()
    }

    @Test
    fun getTodoById() {
        verify { storage.observeById(taskId) }
    }

    @Test
    fun observeDataFromStorage() {
        val result = observer.values().last()
        assertEquals(todo, result)
    }
}