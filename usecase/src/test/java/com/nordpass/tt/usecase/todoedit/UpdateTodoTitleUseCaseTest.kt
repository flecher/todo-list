package com.nordpass.tt.usecase.todoedit

import com.nordpass.tt.usecase.todolist.TodoStorage
import com.nordpass.tt.usecase.utils.OffsetDateTimeProvider
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class UpdateTodoTitleUseCaseTest {

    private lateinit var useCase: UpdateTodoTitleUseCase

    private val storage = mockk<TodoStorage>()
    private val dateTimeProvider = mockk<OffsetDateTimeProvider>()

    @BeforeEach
    fun setUp() {
        every { storage.updateTitleById(any(), any(), any()) } returns Completable.complete()
        every { dateTimeProvider() } returns mockk()
        useCase = UpdateTodoTitleUseCase(storage, dateTimeProvider)
        useCase(1, "test")
    }

    @Test
    fun updateTitleIsCalled() {
        verify { storage.updateTitleById(1, "test", any()) }
    }
}