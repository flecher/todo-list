package com.nordpass.tt.usecase.todolist

import com.nordpass.tt.usecase.Todo
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class GetTodoItemUseCaseTest {

    private val storage = mockk<TodoStorage>()
    private val todo = mockk<Todo>()

    private lateinit var useCase: GetTodoItemUseCase
    private lateinit var observer: TestObserver<Todo>

    private val taskId = 1

    @BeforeEach
    fun setUp() {
        every { storage.getById(any()) } returns Single.just(todo)
        useCase = GetTodoItemUseCase(storage)
        observer = useCase.get(taskId).test()
    }

    @Test
    fun getTodoById() {
        verify { storage.getById(taskId) }
    }

    @Test
    fun observeDataFromStorage() {
        val result = observer.values().last()
        assertEquals(todo, result)
    }

}