package com.nordpass.tt.usecase.todolist

import com.nordpass.tt.usecase.Todo
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class GetTodoListUseCaseTest {

    private val storage = mockk<TodoStorage>()
    private val todo = mockk<Todo>()

    private lateinit var useCase: GetTodoListUseCase

    @BeforeEach
    fun setUp() {
        useCase = GetTodoListUseCase(storage)
    }

    @Nested
    inner class GetList {

        private lateinit var observer: TestObserver<List<Todo>>

        @BeforeEach
        fun setUp() {
            every { storage.getAll() } returns Single.just(listOf(todo))
            observer = useCase.get().test()
        }

        @Test
        fun getListCalled() {
            verify { storage.getAll() }
        }

        @Test
        fun receivedTodoList() {
            val results = observer.values().last()
            assertEquals(listOf(todo), results)
        }
    }

    @Nested
    inner class ObserveList {

        private lateinit var observer: TestObserver<List<Todo>>

        @BeforeEach
        fun setUp() {
            every { storage.observeAll() } returns Observable.just(listOf(todo))
            observer = useCase.observe().test()
        }

        @Test
        fun getListCalled() {
            verify { storage.observeAll() }
        }

        @Test
        fun receivedTodoList() {
            val results = observer.values().last()
            assertEquals(listOf(todo), results)
        }
    }
}