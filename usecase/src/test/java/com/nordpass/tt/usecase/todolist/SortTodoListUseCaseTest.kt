package com.nordpass.tt.usecase.todolist

import com.nordpass.tt.usecase.Todo
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class SortTodoListUseCaseTest {

    private lateinit var useCase: SortTodoListUseCase

    @Nested
    inner class SortByIsComplete {

        private val todo1 = mockk<Todo> {
            every { isCompleted } returns true
            every { updatedAt } returns "2021-05-06T03:50:03.771+05:30"
        }

        private val todo2 = mockk<Todo> {
            every { isCompleted } returns false
            every { updatedAt } returns "2021-05-06T03:50:03.771+05:30"
        }

        var list = listOf(todo1, todo2)

        @BeforeEach
        fun setUp() {
            useCase = SortTodoListUseCase()
            list = useCase(list)
        }

        @Test
        fun secondItemIsNowFirst() {
            assertEquals(todo2, list.first())
        }
    }

    @Nested
    inner class SortByUpdatedAt {
        private val todo1 = mockk<Todo> {
            every { isCompleted } returns true
            every { updatedAt } returns "2021-05-06T03:50:03.771+05:30"
        }

        private val todo2 = mockk<Todo> {
            every { isCompleted } returns true
            every { updatedAt } returns "2021-05-06T04:50:03.771+05:30"
        }

        var list = listOf(todo1, todo2)

        @BeforeEach
        fun setUp() {
            useCase = SortTodoListUseCase()
            list = useCase(list)
        }

        @Test
        fun secondItemIsNowFirst() {
            assertEquals(todo2, list.first())
        }
    }

    @Nested
    inner class SortByIsCompletedAndUpdatedAt {

        private val todo1 = mockk<Todo> {
            every { isCompleted } returns true
            every { updatedAt } returns "2021-05-06T03:50:03.771+05:30"
        }

        private val todo2 = mockk<Todo> {
            every { isCompleted } returns false
            every { updatedAt } returns "2021-05-06T03:50:03.771+05:30"
        }

        private val todo3 = mockk<Todo> {
            every { isCompleted } returns true
            every { updatedAt } returns "2021-05-06T04:50:03.771+05:30"
        }

        private val todo4 = mockk<Todo> {
            every { isCompleted } returns false
            every { updatedAt } returns "2021-05-06T04:50:03.771+05:30"
        }

        var list = listOf(todo1, todo2, todo3, todo4)

        @BeforeEach
        fun setUp() {
            useCase = SortTodoListUseCase()
            list = useCase(list)
        }

        @Test
        fun orderIsCorrect() {
            assertEquals(todo1, list.last())
            assertEquals(todo4, list.first())
            assertEquals(todo2, list[1])
            assertEquals(todo3, list[2])
        }

    }

    @Nested
    inner class SortEmptyList {

        var list = listOf<Todo>()

        @BeforeEach
        fun setUp() {
            useCase = SortTodoListUseCase()
            list = useCase(list)
        }

        @Test
        fun returnsEmptyList() {
            assertEquals(emptyList<Todo>(), list)
        }
    }
}