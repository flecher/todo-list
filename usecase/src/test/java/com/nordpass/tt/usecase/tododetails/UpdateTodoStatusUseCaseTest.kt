package com.nordpass.tt.usecase.tododetails

import com.nordpass.tt.usecase.todolist.TodoStorage
import com.nordpass.tt.usecase.utils.OffsetDateTimeProvider
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class UpdateTodoStatusUseCaseTest {

    private lateinit var useCase: UpdateTodoStatusUseCase

    private val storage = mockk<TodoStorage>()
    private val dateTimeProvider = mockk<OffsetDateTimeProvider>()

    @BeforeEach
    fun setUp() {
        every { storage.updateStatusById(any(), any(), any()) } returns Completable.complete()
        every { dateTimeProvider() } returns mockk()
        useCase = UpdateTodoStatusUseCase(storage, dateTimeProvider)
        useCase(1, true)
    }

    @Test
    fun updateTodoStatusIsCalled() {
        verify { storage.updateStatusById(1, true, any()) }
    }
}