package com.nordpass.tt.usecase.todolist

import com.nordpass.tt.usecase.Todo
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetTodoListUseCase @Inject constructor(
    private val storage: TodoStorage
) {
    fun get(): Single<List<Todo>> {
        return storage.getAll()
    }

    fun observe(): Observable<List<Todo>> {
        return storage.observeAll()
    }
}