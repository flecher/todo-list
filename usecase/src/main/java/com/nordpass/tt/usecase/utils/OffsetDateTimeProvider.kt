package com.nordpass.tt.usecase.utils

import org.threeten.bp.OffsetDateTime
import javax.inject.Inject

class OffsetDateTimeProvider @Inject constructor() {

    operator fun invoke(): OffsetDateTime {
        return OffsetDateTime.now()
    }
}