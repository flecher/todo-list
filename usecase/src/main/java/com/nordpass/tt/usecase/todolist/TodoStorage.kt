package com.nordpass.tt.usecase.todolist

import com.nordpass.tt.usecase.Todo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface TodoStorage {
    fun save(todoList: List<Todo>): Completable

    fun observeAll(): Observable<List<Todo>>

    fun getAll(): Single<List<Todo>>

    fun getById(id: Int): Single<Todo>

    fun observeById(id: Int): Observable<Todo>

    fun removeById(ids: List<Int>): Completable

    fun updateStatusById(id: Int, completed: Boolean, updatedAt: String): Completable

    fun updateTitleById(id: Int, title: String, updatedAt: String): Completable
}