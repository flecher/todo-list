package com.nordpass.tt.usecase.tododetails

import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.todolist.TodoStorage
import io.reactivex.Observable
import javax.inject.Inject

class GetTodoUseCase @Inject constructor(
    private val storage: TodoStorage
) {
    operator fun invoke(id: Int): Observable<Todo> {
        return storage.observeById(id)
    }
}