package com.nordpass.tt.usecase.tododetails

import com.nordpass.tt.usecase.todolist.TodoStorage
import com.nordpass.tt.usecase.utils.OffsetDateTimeProvider
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.OffsetDateTime
import javax.inject.Inject

class UpdateTodoStatusUseCase @Inject constructor(
    private val storage: TodoStorage,
    private val offsetDateTimeProvider: OffsetDateTimeProvider
) {
    operator fun invoke(id: Int, isCompleted: Boolean): Completable {
        return storage.updateStatusById(id, isCompleted, offsetDateTimeProvider.toString())
    }
}