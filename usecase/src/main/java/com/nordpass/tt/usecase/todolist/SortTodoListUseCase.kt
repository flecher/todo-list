package com.nordpass.tt.usecase.todolist

import com.nordpass.tt.usecase.Todo
import com.nordpass.tt.usecase.utils.toDate
import javax.inject.Inject

class SortTodoListUseCase @Inject constructor() {
    operator fun invoke(list: List<Todo>): List<Todo> {
        return list.sortedWith(compareBy<Todo> { it.isCompleted }.thenByDescending { it.updatedAt.toDate() })
    }
}