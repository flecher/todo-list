package com.nordpass.tt.usecase.todoedit

import com.nordpass.tt.usecase.todolist.TodoStorage
import com.nordpass.tt.usecase.utils.OffsetDateTimeProvider
import io.reactivex.Completable
import javax.inject.Inject

class UpdateTodoTitleUseCase @Inject constructor(
    private val storage: TodoStorage,
    private val offsetDateTimeProvider: OffsetDateTimeProvider
) {
    operator fun invoke(id: Int, title: String): Completable {
        return storage.updateTitleById(id, title, offsetDateTimeProvider.toString())
    }
}